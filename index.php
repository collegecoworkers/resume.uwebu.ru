<?php 
$main_info = array(
	array(
		'label' => 'Основная информация *',
		'fields' => array(
			array('type'=>'input', 'label'=>'Имя', 'placehold'=>'Любимова', 'name'=>'firstname'),
			array('type'=>'input', 'label'=>'Фамилия', 'placehold'=>'Анна', 'name'=>'lastname'),
			array('type'=>'input', 'label'=>'Email', 'placehold'=>'mail@mail.com', 'name'=>'email'),
			array('type'=>'input', 'label'=>'Цель', 'placehold'=>'Финансовый директор', 'name'=>'goal'),
			array('type'=>'input', 'label'=>'Дата рождения', 'placehold'=>'13 апреля 1984г', 'name'=>'birthday'),
			array('type'=>'input', 'label'=>'Желаемый уровень заработной платы', 'placehold'=>'45 000', 'name'=>'wages'),
			array('type'=>'input', 'label'=>'Контактный телефон', 'placehold'=>'+7 (988) 888 77 999', 'name'=>'phone'),
			array('type'=>'input', 'label'=>'Гражданство', 'placehold'=>'Россия', 'name'=>'citizenship'),
			array('type'=>'input', 'label'=>'Город', 'placehold'=>'Москва', 'name'=>'city'),
			array('type'=>'select', 'label'=>'Переезд в другой город', 'name'=>'move_city', 'options' => ['готов' => 'готов', 'не готов' => 'не готов']),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Опыт работы', 'placehold'=>"ООО Урал, г.Екатеринбург, www.ural.ru\nОптовая торговля строительными материалами\nФинансовый директор\nФевраль 2010 - настоящее время", 'name'=>'experience'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Образование', 'placehold'=>"КНУ им. Тараса Шевченко, Механико-математический факультет, Бакалавр 2003-2007", 'name'=>'education'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Языки', 'placehold'=>"Английский язык - Разговорный", 'name'=>'languages'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Владение компьютером', 'placehold'=>"Уверенный пользователь\nMS Office, 1C (7.7, 8.1, 8.2) Банк-клиент, правовые базы:\nКонсультант+, Гарант", 'name'=>'computer_control'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Личные качества', 'placehold'=>"Трудолюбие, испольнительность, ответственность, организаторские способности.", 'name'=>'about'),
		)
	),
);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Генерация резюме</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="asssets/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<!-- Static navbar -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="">Генерация резюме</a>
				</div>
			</div>
		</nav>
		<h3>Заполните данные → Скачайте резюме(Ctrl + S)</h3>
		<form action="generate.php" method="post">
			<?php foreach ($main_info as $g_item): ?>
				<div class="row">
					<div class="col-md-12">
						<legend><?= $g_item['label']?></legend>
					</div>
					<div class="col-md-6 col-md-push-6">
						<div class="section_description"></div>
					</div>
					<div class="col-md-6 col-md-pull-6">
						<?php foreach ($g_item['fields'] as $item): ?>
							<?php if($item['type'] == 'input'): ?>
								<div class="form-group ">
									<label class="control-label"><?= $item['label']?></label>
									<input type="text" class="form-control" placeholder="<?= $item['placehold']?>" name="<?= $item['name']?>" value="" required>
								</div>
							<?php elseif($item['type'] == 'select'): ?>
								<div class="form-group ">
									<label class="control-label"><?= $item['label']?></label>
									<select name="<?= $item['name']?>" class="form-control">
										<?php foreach ($item['options'] as $key => $value): ?>
											<option value="<?= $value ?>"><?= $key ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php elseif($item['type'] == 'textarea'): ?>
								<div class="form-group ">
									<label class="control-label"><?= $item['label']?></label>
									<textarea class="form-control" rows="<?= $item['rows']?>" placeholder="<?= $item['placehold']?>" name="<?= $item['name']?>" required></textarea>
								</div>
							<?php endif ?>
						<?php endforeach ?>
					</div>
				</div>
			<?php endforeach ?>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-success">Отправить</button>
				</div>
			</div>
		</form>
		<br><br><br><br><br>
	</div>
</body>
</html>