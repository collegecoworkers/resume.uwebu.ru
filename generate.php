<?php 
require_once 'dompdf/autoload.inc.php';

use Dompdf\Dompdf;

function renderPage($path = null, $data = null) {
	if (!is_null($data)) { 
		extract($data);
	}
	ob_start();
	include($path);
	$var=ob_get_contents(); 
	ob_end_clean();
	return $var;
}

$dompdf = new Dompdf();
$dompdf->loadHtml(renderPage('resume.phtml', [
	'firstname' => $_POST['firstname'],
	'lastname' => $_POST['lastname'],
	'email' => $_POST['email'],
	'goal' => $_POST['goal'],
	'birthday' => $_POST['birthday'],
	'wages' => $_POST['wages'],
	'phone' => $_POST['phone'],
	'citizenship' => $_POST['citizenship'],
	'city' => $_POST['city'],
	'move_city' => $_POST['move_city'],
	'experience' => $_POST['experience'],
	'education' => $_POST['education'],
	'languages' => $_POST['languages'],
	'computer_control' => $_POST['computer_control'],
	'about' => $_POST['about'],
]));

$dompdf->setPaper('A4', '');
$dompdf->render();


$dompdf->stream('my resume.pdf', array('Attachment'=>0));